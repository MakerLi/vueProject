// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
//---------引入iview---------------
import iView from 'iview';
import 'iview/dist/styles/iview.css';
Vue.use(iView);
//---------引入iview---------------
//---------封装axios---------------
import axios from 'axios'
import {post,fetch,patch,put} from './provider/http'
//定义全局变量
Vue.prototype.$post=post;
Vue.prototype.$fetch=fetch;
Vue.prototype.$patch=patch;
Vue.prototype.$put=put;
//---------封装axios---------------
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
